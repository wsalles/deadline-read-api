FROM python:3-alpine

RUN apk add --virtual .build-dependencies \
            --no-cache \
            python3-dev \
            build-base \
            linux-headers \
            pcre-dev

RUN apk add --no-cache pcre

WORKDIR /app

COPY ./requirements.txt .
RUN pip install -r requirements.txt

ENV TZ="America/Sao_Paulo"
RUN apk add tzdata

RUN apk del .build-dependencies && rm -rf /var/cache/apk/*

COPY ./static ./static
COPY ./templates ./templates
COPY ./app.py .
COPY ./wsgi.ini .
COPY ./wsgi.py .

EXPOSE 8080

CMD ["uwsgi", "--ini", "/app/wsgi.ini"]

