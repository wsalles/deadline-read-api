import redis
import json
from flask import Flask, jsonify, request, render_template

app = Flask(__name__)

enum_sites = {'deadline_emissora': 0, 'deadline_estudios': 1, 'deadline_jor_esp': 2, 'deadline_com_pro': 3}

def inserir_dados_redis(banco, tarefa, dados):
    r = redis.StrictRedis(host='10.7.118.220', decode_responses=True, db=int(banco))
    return r.hmset(tarefa, dados)

def pegar_dados_redis(banco, tarefa):
    r = redis.StrictRedis(host='10.7.118.220', decode_responses=True, db=int(banco))
    return r.hgetall(tarefa)

@app.route('/', methods=["GET"])
def index():
    return render_template('index.html')

# VISUALIZAR OS DADOS DO POST DA WRITE-API
@app.route('/deadline/mongodb', methods=["GET"])
def show_mongo():
    return jsonify({"mongodb": {
        'deadline_emissora': pegar_dados_redis(0, "mongodb"),
        'deadline_estudios': pegar_dados_redis(1, "mongodb"),
        'deadline_jor_esp': pegar_dados_redis(2, "mongodb"),
        'deadline_com_pro': pegar_dados_redis(3, "mongodb")
    }})

@app.route('/deadline/jobs', methods=["GET"])
def show_jobs():
    return jsonify({"jobs": {
        'deadline_emissora': pegar_dados_redis(0, "jobs"),
        'deadline_estudios': pegar_dados_redis(1, "jobs"),
        'deadline_jor_esp': pegar_dados_redis(2, "jobs"),
        'deadline_com_pro': pegar_dados_redis(3, "jobs")
    }})

@app.route('/deadline/slaves', methods=["GET"])
def show_slaves():
    return jsonify({"slaves": {
        'deadline_emissora': pegar_dados_redis(0, "slaves"),
        'deadline_estudios': pegar_dados_redis(1, "slaves"),
        'deadline_jor_esp': pegar_dados_redis(2, "slaves"),
        'deadline_com_pro': pegar_dados_redis(3, "slaves")
    }})

@app.route('/deadline/pulses', methods=["GET"])
def show_pulses():
    return jsonify({"pulses": {
        'deadline_emissora': pegar_dados_redis(0, "pulses"),
        'deadline_estudios': pegar_dados_redis(1, "pulses"),
        'deadline_jor_esp': {
            "manager": pegar_dados_redis(2, "pulses_manager"),
            "stack": pegar_dados_redis(2, "pulses_stack")
        },
        'deadline_com_pro': {
            "manager": pegar_dados_redis(3, "pulses_manager"),
            "stack": pegar_dados_redis(3, "pulses_stack")
        }}
    })

@app.route('/deadline/licensesForwarders', methods=["GET"])
def show_licenses():
    return jsonify({"licensesForwarders": {
        'deadline_emissora': pegar_dados_redis(0, "licensesForwarders"),
        'deadline_estudios': pegar_dados_redis(1, "licensesForwarders"),
        'deadline_jor_esp': pegar_dados_redis(2, "licensesForwarders"),
        'deadline_com_pro': pegar_dados_redis(3, "licensesForwarders")
    }})

# POST RECEBIDOS PELA WRITE-API
@app.route('/deadline/mongodb/cadastrar/<string:site>', methods=["POST"])
def input_mongo(site):
    dados = request.get_json()
    try:
        inserir_dados_redis(enum_sites[site], "mongodb", dados)
        return jsonify({"msg": "DADOS INSERIDOS COM SUCESSO!"}), 201
    except Exception as e:
        print(e)
        return jsonify({"msg": "ERRO AO INSERIR OS DADOS"}), 400

@app.route('/deadline/jobs/cadastrar/<string:site>', methods=["POST"])
def input_jobs(site):
    dados = request.get_json()
    try:
        inserir_dados_redis(enum_sites[site], "jobs", dados)
        return jsonify({"msg": "DADOS INSERIDOS COM SUCESSO!"}), 201
    except Exception as e:
        print(e)
        return jsonify({"msg": "ERRO AO INSERIR OS DADOS", "descricao": e}), 400

@app.route('/deadline/slaves/cadastrar/<string:site>', methods=["POST"])
def input_slaves(site):
    dados = request.get_json()
    try:
        inserir_dados_redis(enum_sites[site], "slaves", dados)
        return jsonify({"msg": "DADOS INSERIDOS COM SUCESSO!"}), 201
    except Exception as e:
        print(e)
        return jsonify({"msg": "ERRO AO INSERIR OS DADOS", "descricao": e}), 400

@app.route('/deadline/<string:papel>/cadastrar/<string:site>', methods=["POST"])
def input_pulses(site, papel):
    dados = request.get_json()
    try:
        inserir_dados_redis(enum_sites[site], papel, dados)
        return jsonify({"msg": "DADOS INSERIDOS COM SUCESSO!"}), 201
    except Exception as e:
        print(e)
        return jsonify({"msg": "ERRO AO INSERIR OS DADOS", "descricao": e}), 400

@app.route('/deadline/licensesForwarders/cadastrar/<string:site>', methods=["POST"])
def input_licenses(site):
    dados = request.get_json()
    try:
        inserir_dados_redis(enum_sites[site], "licensesForwarders", dados)
        return jsonify({"msg": "DADOS INSERIDOS COM SUCESSO!"}), 201
    except Exception as e:
        return jsonify({"msg": "ERRO AO INSERIR OS DADOS", "descricao": e}), 400

if __name__ == '__main__':
    #app.run(host='0.0.0.0')
    app.run(host='0.0.0.0', port=80, debug=True)